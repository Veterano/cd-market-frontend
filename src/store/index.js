import Vue from 'vue'
import Vuex from 'vuex'
import vs from '../../package.json'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        versao: vs.version,
        msg: '',
        login: [],
        page: 1,
        menu: [],
        descartes: [],
        estoque:[],
        resultado: [],
        usuario: {
            userbase: '',
            nome: '',
            email: 'julia.melo',
            senha: '10293847',
            token: '',
            adm: false,
            logado: false
        },
        login: [],
        movimentacoes: [],
        valores: [],
        nomePagina: '',
    }
})
 